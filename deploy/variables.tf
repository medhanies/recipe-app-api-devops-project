variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "medhaniesolomon@yahoo.com"
}

variable "db_username" {
  description = "username for the rds postgres instance"
}

variable "db_password" {
  description = "password for the rds postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "259585693254.dkr.ecr.us-east-1.amazonaws.com/app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "259585693254.dkr.ecr.us-east-1.amazonaws.com/django-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "secret key for django app"
}